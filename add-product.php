<?php require 'models/config.php'; ?>
<?php 
  if (isset($_POST['sku'])) {
    extract($_POST);
    $sku= cf::clean_input($sku);
    $name= cf::clean_input($name);
    $price= cf::clean_input($price);
    $type= cf::clean_input($type);
    $size= cf::clean_input($size);
    $weight= cf::clean_input($weight);
    $length= cf::clean_input($length);
    $width= cf::clean_input($width);
    $height= cf::clean_input($height);
    dbi::product($sku,$name,$price,$type,$size,$weight,$length,$width,$height,time());
    cf::redirect('./');
  }
 ?>
<!DOCTYPE html>
<html>
<title>Product Add</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<body>

  <header>
    <div class="w3-container w3-row w3-margin" style="border-bottom: 3px solid black;">
      <div class="w3-col m10">
        <h2>Product Add</h2>
      </div> 
      <div class="w3-col m2 w3-padding-24 w3-right-align">
        <button class="btn btn-primary" onclick="saveProduct()" name="save">Save</button>
        <a href="./"><button class="btn btn-primary" id="delete-product-btn">Cancel</button></a>
      </div>
    </div>
  </header>

  <div class="w3-container w3-row w3-margin" style="min-height: 70vh;">
    <p style="display: none;" id="missing_message">Please, submit required data.</p>
    <div class="w3-col m6">
      <form id="product_form" action="" method="post" name="myform">
        <div class="w3-row w3-margin-bottom">
          <div class="w3-col m4">
            <label>SKU</label>
          </div>
          <div class="w3-col m8">
            <input type="text" name="sku" class="w3-input w3-border" id="sku">
          </div>
        </div>
        <div class="w3-row w3-margin-bottom">
          <div class="w3-col m4">
            <label>Name</label>
          </div>
          <div class="w3-col m8">
            <input type="text" name="name" class="w3-input w3-border" id="name">
          </div>
        </div>
        <div class="w3-row w3-margin-bottom">
          <div class="w3-col m4">
            <label>Price ($)</label>
          </div>
          <div class="w3-col m8">
            <input type="number" name="price" class="w3-input w3-border" id="price">
          </div>
        </div>
        <div class="w3-row w3-margin-bottom">
          <div class="w3-col m4">
            <label>Type Switcher</label>
          </div>
          <div class="w3-col m8">
            <select class="w3-select w3-border" name="type" onchange="typeSwitcher()" id="productType">
              <option value="" disabled selected>Type Switcher</option>
              <option value="dvd">DVD</option>
              <option value="book">Book</option>
              <option value="furniture">Furniture</option>
            </select>
          </div>
        </div>

        <!-- type switcher options -->
        <div class="w3-row w3-margin-bottom" id="dvd" style="display: none;">
          <div class="w3-col m4">
            <label>Size (MB)</label>
          </div>
          <div class="w3-col m8">
            <input type="number" name="size" class="w3-input w3-border" id="size">
          </div>
        </div>

        <div id="furniture" style="display: none;">
          <div class="w3-row w3-margin-bottom">
            <div class="w3-col m4">
              <label>Height (CM)</label>
            </div>
            <div class="w3-col m8">
              <input type="number" name="height" class="w3-input w3-border" id="height">
            </div>
          </div>
          <div class="w3-row w3-margin-bottom">
            <div class="w3-col m4">
              <label>Width (CM)</label>
            </div>
            <div class="w3-col m8">
              <input type="number" name="width" class="w3-input w3-border" id="width">
            </div>
          </div>
          <div class="w3-row w3-margin-bottom">
            <div class="w3-col m4">
              <label>Length (CM)</label>
            </div>
            <div class="w3-col m8">
              <input type="number" name="length" class="w3-input w3-border" id="length">
            </div>
          </div>
        </div>

        <div class="w3-row w3-margin-bottom" id="book" style="display: none;">
          <div class="w3-col m4">
            <label>Weight (KG)</label>
          </div>
          <div class="w3-col m8">
            <input type="number" name="weight" class="w3-input w3-border" id="weight">
          </div>
        </div>
      </form>
    </div>
    
  </div>

  <footer class="w3-padding-16 w3-center w3-margin" style="border-top: 3px solid black;">
    <div class="w3-container">
      <h6>Scandiweb Test assignment</h6>
    </div>
  </footer>

  <script type="text/javascript">
    function typeSwitcher(){
      var x= document.getElementById('productType').value;
      if(x== 'dvd'){
        document.getElementById('dvd').style.display= 'block';
        document.getElementById('furniture').style.display= 'none';
        document.getElementById('book').style.display= 'none';
        document.getElementById('weight').value= 0;
        document.getElementById('height').value= 0;
        document.getElementById('width').value= 0;
        document.getElementById('length').value= 0;
      }else if(x== 'book'){
        document.getElementById('dvd').style.display= 'none';
        document.getElementById('furniture').style.display= 'none';
        document.getElementById('book').style.display= 'block';
        document.getElementById('size').value= 0;
        document.getElementById('height').value= 0;
        document.getElementById('width').value= 0;
        document.getElementById('length').value= 0;
      }else if(x== 'furniture'){
        document.getElementById('dvd').style.display= 'none';
        document.getElementById('furniture').style.display= 'block';
        document.getElementById('book').style.display= 'none';
        document.getElementById('size').value= 0;
        document.getElementById('weight').value= 0;
      }
    }

    function saveProduct() {
      var sku= document.getElementById('sku').value;
      var name= document.getElementById('name').value;
      var price= document.getElementById('price').value;
      var switcher= document.getElementById('productType').value;
      var size= document.getElementById('size').value;
      var height= document.getElementById('height').value;
      var width= document.getElementById('width').value;
      var length= document.getElementById('length').value;
      var weight= document.getElementById('weight').value;
      if (sku== '' || name== '' || price=='' || switcher== '' || size== '' || weight== '' || height== '' || width== '' || length== '') {
        document.getElementById('missing_message').style.display= 'block';
      }else{
        document.getElementById("product_form").submit();
      }
    }
  </script>
</body>
</html>
