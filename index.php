<?php require 'models/config.php'; ?>
<?php 
  if (isset($_POST['product_id'])) {
    extract($_POST);
    foreach($product_id as $id){
      $stmt = $db->query("DELETE FROM product WHERE id= $id");
    }
  }
 ?>
<!DOCTYPE html>
<html>
<title>Product List</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<body>

  <header>
    <div class="w3-container w3-row w3-margin" style="border-bottom: 3px solid black;">
      <div class="w3-col m10 s9">
        <h2>Product List</h2>
      </div> 
      <div class="w3-col m2 s3 w3-padding-24 w3-right-align">
        <a href="add-product"><button class="btn btn-primary" type="button">ADD</button></a>
        <button class="btn btn-primary" id="delete-product-btn" onclick="massDelete()" type="button">MASS DELETE</button>
      </div>
    </div>
  </header>

  <form action="" method="post" id="productForm">
  <div class="w3-container w3-row-padding w3-margin" style="min-height: 70vh;">
    <?php 
      $no = "";
      $stmt = $db->query("SELECT * FROM product ORDER BY id DESC");
      while ($product = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $product_id = $beneficiary['product_id'];
      if ($product['type']== 'dvd') {
        $type= 'Size';
        $unit= $product['size'].' MB';
      }elseif ($product['type']== 'book') {
        $type= 'Weight';
        $unit= $product['weight'].' KG';
      }elseif ($product['type']== 'furniture') {
        $type= 'Dimension';
        $unit= $product['length'].'X'.$product['width'].'X'.$product['height'];
      }
    ?>
    <div class="w3-col m3">
      <div style="border: 4px solid black;padding: 15px 15px 35px 15px;">
        <div>
          <input class="w3-check delete-checkbox" type="checkbox" name="product_id[]" value="<?php echo $product['id'] ?>">
        </div>
        <div class="w3-center">
          <p><?php echo $product['sku'] ?> <br>
          <?php echo $product['name'] ?> <br>
          <?php echo number_format($product['price'],2) ?> $ <br>
          <?php echo $type ?>: <?php echo $unit ?></p>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
    </form>
 
  <footer class="w3-padding-16 w3-center w3-margin" style="border-top: 3px solid black;">
    <div class="w3-container">
      <h6>Scandiweb Test assignment</h6>
    </div>
  </footer>

  <script type="text/javascript">
    function massDelete() {
      document.getElementById("productForm").submit();
    }
  </script>
</body>
</html>
